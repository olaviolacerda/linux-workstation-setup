***NOTE: There are some configs and tutorials here extracted from others websites and Github users (https://github.com/Rocketseat/ambiente-react-native, https://github.com/leonardoazeredo, Insomnia, https://code.visualstudio.com/docs, Genymotion, Android), as I said, it's the full configuration for my workstation, so use it for the good.***

# Initial-linux-configs
A personal guide to configure the full development environment with Node, MongoDB, VSCode, Insomnia and ReactJS

# System dependencies

- Install Linux Dependencies 

```bash
sudo apt-get update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev yarn libpq-dev -y
```

- Install Node.js 10.x and some development tools to build native addons
  
```bash
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install nodejs gcc g++ make -y
```

- To install the Yarn package manager, run:
```bash
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```

- Configure Git

```bash
git config --global color.ui true
git config --global user.name "username"
git config --global user.email "username@email.com"
ssh-keygen -t rsa -b 4096 -C "username@email.com"
git config credential.helper store
```

- The next step is to take the newly generated SSH key and add it to your Github account. You want to copy and paste the output of the following command and paste it here(https://github.com/settings/ssh).

```bash
cat ~/.ssh/id_rsa.pub
```

- Once you've done this, you can check and see if it worked:

```bash
ssh -T git@github.com
```

- You should get a message like this:

```bash
Hi YOURNAME! You've successfully authenticated, but GitHub does not provide shell access.
```

# MongoDB

- Import the public key used by the package management system. From a terminal, issue the following command to import the MongoDB public GPG Key from https://www.mongodb.org/static/pgp/server-3.2.asc:

`wget -qO - https://www.mongodb.org/static/pgp/server-3.2.asc | sudo apt-key add -` 

The operation should respond with an OK.

- Create a list file for MongoDB. Create the `/etc/apt/sources.list.d/mongodb-org-3.2.list` list file using the command appropriate for your version of Ubuntu: 
 ```sh
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
```
- Reload local package database.
Issue the following command to reload the local package database: `sudo apt-get update`

- Install the MongoDB packages. You can install either the latest stable version of MongoDB or a specific version of MongoDB. Install the latest stable version of MongoDB. Issue the following command: `sudo apt-get install -y mongodb-org` 

- Install a specific release of MongoDB. To install a specific release, you must specify each component package individually along with the version number, as in the following example:
```sh 
sudo apt-get install -y mongodb-org=3.2.22 mongodb-org-server=3.2.22 mongodb-org-shell=3.2.22 mongodb-org-mongos=3.2.22 mongodb-org-tools=3.2.22```
If you only install mongodb-org=3.2.22 and do not include the component packages, the latest version of each MongoDB package will be installed regardless of what version you specified.

- Pin a specific version of MongoDB. Although you can specify any available version of MongoDB, apt-get will upgrade the packages when a newer version becomes available. To prevent unintended upgrades, pin the package. To pin the version of MongoDB at the currently installed version, issue the following command sequence:
```sh
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
```

# ReactJS Configuration

- The ReactJS only needs the NodeJS to run, but you can need to install the create-react-app package.

```bash
sudo npm install -g create-react-app -y
```
 
# VSCode

- Fira Code (https://github.com/tonsky/FiraCode)

- Dev Fonts (https://github.com/hbin/top-programming-fonts)

```bash
curl -L https://github.com/hbin/top-programming-fonts/raw/master/install.sh | bash
```

- Visual Studio Code (https://code.visualstudio.com/docs/setup/linux)


*If you have some troubles with VS Code file watcher, follow the steps below:*

```bash
cat /proc/sys/fs/inotify/max_user_watches
```

Then, go to `/etc/sysctl.conf` and add this line to the end of the file:

`fs.inotify.max_user_watches=524288`

The new value can then be loaded in by running `sudo sysctl -p`.


For VSCode extensions, I use those Settings: 

```json
{
  "editor.fontFamily": "Operator Mono Book, Fira Code, 'Droid Sans Mono', 'monospace', monospace, 'Droid Sans Fallback'",
  "editor.fontLigatures": true,
  "workbench.startupEditor": "newUntitledFile",
  "terminal.integrated.cursorStyle": "line",
  "terminal.integrated.cursorBlinking": true,
  "editor.occurrencesHighlight": false,
  "editor.selectionHighlight": true,
  "files.watcherExclude": {
    "**/.git/objects/**": true,
    "**/.git/subtree-cache/**": true,
    "**/node_modules/*/**": true
  },
  "editor.cursorBlinking": "smooth",
  "editor.cursorSmoothCaretAnimation": true,
  "workbench.colorTheme": "Night Owl",
  "explorer.confirmDragAndDrop": false,
  "files.associations": {
    "*.js": "javascript"
  },
  "javascript.updateImportsOnFileMove.enabled": "always",
  "jest.showCoverageOnLoad": true,
  "typescript.updateImportsOnFileMove.enabled": "always",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "javascript.suggest.completeFunctionCalls": true,
  "editor.fontSize": 14,
   "editor.cursorBlinking": "smooth",
  "editor.cursorSmoothCaretAnimation": true,
  "workbench.iconTheme": "material-icon-theme",
  "eslint.autoFixOnSave": true,
  "terminal.integrated.shell.osx": "/bin/zsh",
  "explorer.confirmDelete": false,
  "emmet.syntaxProfiles": {
    "javascript": "jsx"
  },
  "emmet.includeLanguages": {
    "javascript": "javascriptreact"
  },
  "editor.tabSize": 2,
  "editor.detectIndentation": false,
  "editor.formatOnSave": false,
  "window.zoomLevel": 0,
  "files.trimTrailingWhitespace": true,
  "npm.validate.enable": false,
  "[javascript]": {
    "editor.tabSize": 2
  },
  "[typescript]": {
    "editor.tabSize": 2
  }
}
```

And those Extensions, just put at Terminal: 
```bash
#remove all extensions
sudo rm -rf $HOME/.vscode-insiders/

#install new extensions
code --install-extension alefragnani.project-manager
code --install-extension christian-kohler.npm-intellisense
code --install-extension christian-kohler.path-intellisense
code --install-extension CoenraadS.bracket-pair-colorizer-2
code --install-extension dbaeumer.vscode-eslint
code --install-extension eamodio.gitlens
code --install-extension EditorConfig.EditorConfig
code --install-extension eg2.vscode-npm-script
code --install-extension formulahendry.auto-close-tag
code --install-extension MaoSantaella.night-wolf
code --install-extension ms-azuretools.vscode-docker
code --install-extension naumovs.color-highlight
code --install-extension Orta.vscode-jest
code --install-extension PKief.material-icon-theme
code --install-extension WallabyJs.quokka-vscode
```

# Insomnia (REST Client) (https://support.insomnia.rest/article/23-installation#ubuntu)

```bash
# Add to sources
echo "deb https://dl.bintray.com/getinsomnia/Insomnia /" \
    | sudo tee -a /etc/apt/sources.list.d/insomnia.list

# Add public key used to verify code signature
wget --quiet -O - https://insomnia.rest/keys/debian-public.key.asc \
    | sudo apt-key add -

# Refresh repository sources and install Insomnia
sudo apt-get update
sudo apt-get install insomnia
```

# Mongo and MySQL with Docker Compose

```yml
version: '3'
services:
    mongo:
        image: mongo
        restart: always
        container_name: mongo
        environment:
            MONGO_INITDB_ROOT_USERNAME: root
            MONGO_INITDB_ROOT_PASSWORD: root
        volumes:
            - '/home/olaviosemacento/volumes/mongo:/data/db'
        ports:
            - '27017:27017'
    mysql:
        image: 'mysql:5.7'
        restart: always
        container_name: mysql
        environment:
            MYSQL_DATABASE: db
            MYSQL_ROOT_USER: root
            MYSQL_ROOT_PASSWORD: root
        volumes:
            - '/home/olaviosemacento/volumes/mysql:/var/lib/mysql'
        ports:
            - '3306:3306'

```



